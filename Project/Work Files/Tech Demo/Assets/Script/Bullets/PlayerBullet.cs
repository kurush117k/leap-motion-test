﻿using UnityEngine;
using System.Collections;

public class PlayerBullet : BaseBullet 
{

	protected override void Initialization ()
	{
		base.Initialization ();
		m_moveSpeed = -40.0f;
		m_idTagOne = "Charger";
		m_idTagTwo = "Shooter";
	}
}
