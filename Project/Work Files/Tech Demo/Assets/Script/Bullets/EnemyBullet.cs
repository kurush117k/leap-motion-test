﻿using UnityEngine;
using System.Collections;

public class EnemyBullet : BaseBullet 
{
	
	protected override void Initialization ()
	{
		base.Initialization ();
		m_moveSpeed = 30.0f;
		m_idTagOne = "Player";
		m_idTagTwo = "SmartBomb";
	}
}
