﻿using UnityEngine;
using System.Collections;

public class SpawnArray : MonoBehaviour 
{
	[SerializeField] protected GameObject[] m_spawnArray;

	protected GameObject[] m_shooterArray;
	protected GameObject[] m_chargerArray;
	protected float m_currentTime;
	protected float m_oldTime;
	protected float m_spawnTime;
	protected float m_spawnOffset;
	protected int m_spawnID;
	protected int m_numebrToSpawn;
	protected Vector3 m_spawnPos;


	// Use this for initialization
	protected void Start () 
	{
		Initialization ();
	}

	protected void Initialization ()
	{
		m_currentTime = Time.time;
		m_oldTime = m_currentTime;
		m_spawnOffset = 3.5f;
		m_numebrToSpawn = 7;
		m_shooterArray = new GameObject[m_numebrToSpawn];
		m_chargerArray = new GameObject[m_numebrToSpawn];

		for (int i = 0; i < m_numebrToSpawn; i++)
		{
			m_shooterArray[i] = Instantiate(m_spawnArray[0]) as GameObject;
			m_shooterArray[i].SetActiveRecursively (false);
			m_chargerArray[i] = Instantiate(m_spawnArray[1]) as GameObject;
			m_chargerArray[i].SetActiveRecursively (false);
		}
	}
	
	// Update is called once per frame
	protected void Update () 
	{
		SpawnObject ();
	}

	protected void SpawnObject ()
	{
		m_currentTime = Time.time;
		m_spawnTime = m_currentTime - m_oldTime ;

		if (m_spawnTime >= m_spawnOffset)
		{
			for (int i = 0; i < m_spawnArray.Length; i++)
			{
				m_spawnID = Random.Range(0,2);

				if (m_spawnID == 0)
				{
					for (int n = 0; n < m_numebrToSpawn; n++)
					{
						if (m_shooterArray[n].active == false)
						{
							m_shooterArray[n].SetActiveRecursively (true);
							m_shooterArray[n].GetComponent<BaseEnemy> ().Initialization ();
							m_spawnPos.y = Random.Range(-10.5f, 0.0f);
							m_spawnPos.x = transform.position.x;
							m_spawnPos.z = transform.position.z;
							m_shooterArray[n].transform.position = m_spawnPos;
							n = m_numebrToSpawn;
						}
					}
				}
				else if (m_spawnID == 1)
				{
					for (int n = 0; n < m_numebrToSpawn; n++)
					{
						if (m_shooterArray[n].active == false)
						{
							m_chargerArray[n].SetActiveRecursively (true);
							m_chargerArray[n].GetComponent<BaseEnemy> ().Initialization ();
							m_spawnPos.y = Random.Range(-21.0f, -10.6f);
							m_spawnPos.x = transform.position.x;
							m_spawnPos.z = transform.position.z;
							m_chargerArray[n].transform.position = m_spawnPos;
							n = m_numebrToSpawn;
						}
					}
				}
			}

			m_spawnOffset = m_spawnOffset - 0.5f;
			m_oldTime = Time.time;
		}
	}
}