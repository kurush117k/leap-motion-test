﻿using UnityEngine;
using System.Collections;

public class HealthPickUp : BasePickUp
{
	protected override void Initialization ()
	{
		base.Initialization ();
		m_moveSpeed = 15.0f;
		m_idTagOne = "Player";
	}
}
