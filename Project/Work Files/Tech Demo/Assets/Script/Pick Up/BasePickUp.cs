﻿using UnityEngine;
using System.Collections;

public class BasePickUp : MonoBehaviour 
{
	protected float m_moveSpeed;
	protected string m_idTagOne;
	
	// Use this for initialization
	protected void Start () 
	{
		Initialization ();
	}
	
	// Update is called once per frame
	protected virtual void Update () 
	{
		Move ();
	}
	
	protected virtual void Initialization ()
	{
		m_moveSpeed = 0.0f;
		m_idTagOne = null;
	}
	
	protected virtual void Move ()
	{
		transform.Translate(Vector3.left * m_moveSpeed * Time.deltaTime);
	}
	
	protected void OnCollisionEnter (Collision other)
	{
		if (other.gameObject.tag == "Boundary")
		{
			Destroy (gameObject);
		}
		if (other.gameObject.tag == m_idTagOne)
		{
			Destroy (gameObject);
		}
	}
}
