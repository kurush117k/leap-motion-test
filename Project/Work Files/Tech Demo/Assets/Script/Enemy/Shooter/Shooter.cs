﻿using UnityEngine;
using System.Collections;

public class Shooter : BaseShooter
{
	
	// Update is called once per frame
	protected override void Update () 
	{
		base.Update ();
	}

	public override void Initialization ()
	{
		base.Initialization ();
		m_health = 10;
		m_moveSpeed = 10.0f;
		m_damage = 5;
		m_fireRate = 5.0f;
		m_fireTimer = 0.0f;
	}
}