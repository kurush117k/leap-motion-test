﻿using UnityEngine;
using System.Collections;

public class BaseShooter : Killable
{
	[SerializeField] protected GameObject m_bullet;
	[SerializeField] protected GameObject m_bulletSpawn;
	[SerializeField] protected GameObject m_bulletSound;

	protected float m_fireRate;
	protected float m_fireTimer;

	// Update is called once per frame
	protected override void Update () 
	{
		base.Update ();
		Shoot ();
	}

	public override void Initialization ()
	{
		base.Initialization ();
		m_fireRate = 0.0f;
		m_fireTimer = 0.0f;
	}

	protected virtual void Shoot ()
	{
		if (m_engaged)
		{
			if (m_fireTimer < -5)
			{
				m_fireTimer = -5;
			}

			GameObject m_bulletSoundObject;

			if (Time.timeScale != 0)
			{
				if (m_fireTimer <= 0)
				{
					if (m_bullet)
					{
						Instantiate (m_bullet, m_bulletSpawn.transform.position, m_bulletSpawn.transform.rotation);
					}

					if (m_bulletSound)
					{
						m_bulletSoundObject = Instantiate (m_bulletSound, m_bulletSpawn.transform.position, m_bulletSpawn.transform.rotation) as GameObject;
					}

					m_fireTimer = 1;
				}
			}

			m_fireTimer -= Time.deltaTime * m_fireRate;
		}
	}
}