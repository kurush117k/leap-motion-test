﻿using UnityEngine;
using System.Collections;

public class Charger : BaseCharger
{

	// Update is called once per frame
	protected override void Update () 
	{
		base.Update ();
	}
	
	public override void Initialization ()
	{
		base.Initialization ();
		m_health = 15;
		m_moveSpeed = 15.0f;
		m_damage = 5;
	}
}