﻿using UnityEngine;
using System.Collections;

public class BaseCharger : Killable 
{
	protected float m_normSpeed;

	protected const float M_CHARGESPEED = 25.0f;

	// Update is called once per frame
	protected override void Update () 
	{
		base.Update ();
		Charge ();
	}
	
	public override void Initialization ()
	{
		base.Initialization ();
	}

	protected virtual void Charge ()
	{
		if (m_engaged)
		{
			m_normSpeed = m_moveSpeed;
			m_moveSpeed = M_CHARGESPEED;
		}
		if (!m_engaged && m_normSpeed != 0.0f)
		{
			m_moveSpeed = m_normSpeed;
		}
	}
}
