﻿using UnityEngine;
using System.Collections;

public class Killable : BaseEnemy 
{
	[SerializeField] protected GameObject[] m_pickUp;

	protected int m_spawnID;
	protected int m_damage;
	protected float m_boppHight;
	protected float m_boppSpeed;
	protected float m_boppOffset;
	protected float m_bopp;
	protected GameObject m_pickUpSpawn;

	// Update is called once per frame
	protected override void Update () 
	{
		base.Update ();
		Death ();
	}

	public override void Initialization ()
	{
		base.Initialization ();
		m_damage = 0;
		m_boppSpeed = 7.5f;
		m_boppOffset = 1.0f;
		m_boppHight = 0.1f;
		m_pickUpSpawn = this.gameObject;
	}

	protected override void Move ()
	{
		base.Move ();
		m_bopp = Mathf.Sin(Time.time * m_boppSpeed + m_boppOffset) * m_boppHight;
		gameObject.transform.position = new Vector3 (transform.position.x, transform.position.y + m_bopp, transform.position.z);
	}

	protected virtual void Death ()
	{
		if (m_health <= 0)
		{
			m_spawnID = Random.Range(0,2);
			if (m_pickUp[m_spawnID])
			{
				m_spawnID = Random.Range(0,2);
				Instantiate (m_pickUp[m_spawnID], m_pickUpSpawn.transform.position, m_pickUpSpawn.transform.rotation);
			}
			this.gameObject.SetActiveRecursively (false);
		}
	}

	protected override void OnCollisionEnter (Collision other)
	{
		base.OnCollisionEnter (other);
		if (other.gameObject.tag == "PlayerBullet")
		{
			m_health = m_health - m_damage;
		}
		if (other.gameObject.tag == "Player" || other.gameObject.tag == "SmartBomb")
		{
			m_health = m_health - m_health;
		}
	}	
}