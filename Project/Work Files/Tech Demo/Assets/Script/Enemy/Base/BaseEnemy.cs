﻿using UnityEngine;
using System.Collections;

public class BaseEnemy : MonoBehaviour 
{

	protected float m_moveSpeed;
	protected int m_health;
	protected bool m_engaged;

	protected const float M_VIEWDISTANCE = 30.0f;
	protected const string M_RAYCHECKTAG = "Player"; 
	protected const int M_RAYCHECKID = 1 << 8;

	// Use this for initialization
	protected void Start () 
	{
		Initialization ();
	}
	
	// Update is called once per frame
	protected virtual void Update () 
	{
		Move ();
		Engage ();
	}

	public virtual void Initialization ()
	{
		m_moveSpeed = 0.0f;
		m_health = 0;
		m_engaged = false;
	}

	protected virtual void Move ()
	{
		transform.Translate (Vector3.left * m_moveSpeed * Time.deltaTime);
	}

	protected void Engage ()
	{
		Vector3 m_forRay = transform.TransformDirection(Vector3.left);
		RaycastHit hit;
		
		Debug.DrawRay(transform.position, m_forRay * M_VIEWDISTANCE, Color.red);
		
		if (Physics.Raycast (transform.position, m_forRay, out hit, M_VIEWDISTANCE, M_RAYCHECKID) && hit.transform.gameObject.tag == M_RAYCHECKTAG)
		{
			m_engaged = true;
		}
		if (!Physics.Raycast (transform.position, m_forRay, M_VIEWDISTANCE, M_RAYCHECKID))
		{
			m_engaged = false; 
		}
	}

	protected virtual void OnCollisionEnter (Collision other)
	{
		if (other.gameObject.tag == "Boundary")
		{
			this.gameObject.SetActiveRecursively (false);
		}
	}
}
