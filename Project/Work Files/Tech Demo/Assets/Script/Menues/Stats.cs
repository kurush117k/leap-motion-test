﻿using UnityEngine;
using System.Collections;

public class Stats : MonoBehaviour 
{
	[SerializeField] protected string m_lvlName;

	protected float m_timeLimit;
	protected float m_currentTime;
	protected float m_hudTime;

	// Use this for initialization
	void Start () 
	{
		Initialization ();
	}

	protected void Initialization ()
	{
		m_timeLimit = 45.0f;
		m_currentTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () 
	{
		Timer ();
	}

	protected void Timer ()
	{
		m_currentTime = Time.time;
		m_hudTime = m_timeLimit - m_currentTime;

		if (m_currentTime >= m_timeLimit)
		{
			Application.LoadLevel(m_lvlName);
		}
	}
	
	protected void OnGUI ()
	{
		if (m_hudTime > 10.0f)
		{
			GUI.Label (new Rect (10, 50, 100, 100), m_hudTime.ToString ());
		}
		else 
		{
			GUI.contentColor = Color.red;
			GUI.Label (new Rect (10, 50, 100, 100), m_hudTime.ToString ());
		}
	}
}