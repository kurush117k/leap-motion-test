﻿using UnityEngine;
using System.Collections;

public class BaseMenu : MonoBehaviour 
{
	[SerializeField] protected string[] m_lvlName;

	protected float m_areaHight;
	protected float m_areaWidth;
	protected float m_layoutCenter;
	protected float m_buttonSpacing;
	protected GUISkin m_customSkin;
	protected GUIStyle m_guiStyle;
	protected Texture2D m_textureTop;


	protected void Start () 
	{
		Initialization ();
	}

	protected virtual void Initialization ()
	{
		m_areaHight = 0.0f;
		m_areaWidth = 0.0f;
		m_layoutCenter = 0.0f;
		m_buttonSpacing = 0.0f;
	}

	protected virtual void OnGUI() 
	{

	}
}
