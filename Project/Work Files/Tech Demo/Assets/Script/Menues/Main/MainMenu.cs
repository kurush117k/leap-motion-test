﻿using UnityEngine;
using System.Collections;

public class MainMenu : BaseMenu 
{
	protected override void Initialization ()
	{
		m_areaHight = 200.0f;
		m_areaWidth = 200.0f;
		m_layoutCenter = 100.0f;
		m_buttonSpacing = 25.0f;
	}

	protected override void OnGUI ()
	{
		//GUI.skin = m_customSkin;
		GUILayout.BeginArea (new Rect (Screen.width / 2 - m_areaHight / 2, Screen.height / 2 - m_areaWidth / 2, m_areaWidth, m_areaHight));
		GUILayout.Space(m_buttonSpacing);
		//GUILayout.Label(m_textureTop);
		GUILayout.Space(m_buttonSpacing);

		if(GUILayout.Button("Start Game"))
		{
			Application.LoadLevel(m_lvlName[0]);
		}
		
		GUILayout.Space(m_buttonSpacing);
		
		if(GUILayout.Button("Quit Game"))
		{
			Application.Quit();
		}
		
		GUILayout.EndArea();	
	}
}
