﻿using UnityEngine;
using System.Collections;
using Leap;

public class Player : MonoBehaviour 
{

	[SerializeField] protected GameObject m_bullet;
	[SerializeField] protected GameObject m_smartBomb;
	[SerializeField] protected GameObject m_bulletSpawn;
	[SerializeField] protected GameObject m_bulletSound;
	[SerializeField] protected GameObject[] m_effects;
	[SerializeField] protected GameObject[] m_effectSound;
	[SerializeField] protected Texture[] m_smartBombTexture;
	[SerializeField] protected Texture[] m_healthBarTexture;
	[SerializeField] protected string m_lvlName;
	
	protected int m_health;
	protected int m_life;
	protected int m_smartBombAmount;
	protected bool m_engaged;
	protected float m_moveSpeed;
	protected float m_yLimit;
	protected float m_boppHight;
	protected float m_boppSpeed;
	protected float m_boppOffset;
	protected float m_bopp;
	protected float m_fireRate;
	protected float m_bombRate;
	protected float m_fireTimer;
	protected float m_bombTimer;
	protected Controller m_controller;
	
	protected const float M_VIEWDISTANCE = 40.0f;
	protected const string M_RAYCHECKSHOOTER = "Shooter"; 
	protected const string M_RAYCHECKCHARGER = "Charger";
	protected const int M_RAYCHECKID = 1 << 9;
	protected const int M_MAXSMARTBOMB = 3;
	protected const int M_MAXHEALTH = 125;

	// Use this for initialization
	void Start () 
	{
		Initialization ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		Engage ();
		Move ();
		Shoot ();
		Death ();
		SmartBomb ();
	}

	protected void Initialization ()
	{
		m_health = 100;
		m_life = 3;
		m_smartBombAmount = M_MAXSMARTBOMB;
		m_moveSpeed = 30.0f;
		m_yLimit = -22.0f;
		m_fireRate = 7.5f;
		m_bombRate = 0.5f;
		m_fireTimer = 0.0f;
		m_bombTimer = 0.0f;
		m_boppSpeed = 5.0f;
		m_boppOffset = 2.5f;
		m_boppHight = 0.25f;
		m_engaged = false;
		m_controller = new Controller ();
	}

	protected void Move ()
	{
		if (Input.GetKey(KeyCode.A))
		{
			transform.Translate (Vector3.up * m_moveSpeed * Time.deltaTime);
		}

		if (Input.GetKey(KeyCode.Z))
		{
			transform.Translate (Vector3.down * m_moveSpeed * Time.deltaTime);
		}

		Frame m_controllerFrame = m_controller.Frame ();

		if (m_controllerFrame.IsValid)
		{	
			foreach (Hand m_hand in m_controllerFrame.Hands)
			{
				Vector2 m_trackPosition = ProcessHand (m_controllerFrame, m_hand);
				
				m_trackPosition.y = UnityEngine.Screen.height - m_trackPosition.y;
				
				float m_yPos = m_trackPosition.y;

				m_yPos = (m_yPos / m_moveSpeed) * -1;

				m_bopp = Mathf.Sin(Time.time * m_boppSpeed + m_boppOffset)  * m_boppHight;

				if (m_yPos > m_yLimit)
				{
					gameObject.transform.position = new Vector3 (transform.position.x, m_yPos + m_bopp, transform.position.z); 
				}
			}
		}
		else 
		{
			m_bopp = Mathf.Sin(Time.time * m_boppSpeed + m_boppOffset) * m_boppHight;
			gameObject.transform.position = new Vector3 (transform.position.x, m_bopp, transform.position.z);
		}
	}

	protected void Shoot ()
	{
		if (m_engaged)
		{
			if (m_fireTimer < -5)
			{
				m_fireTimer = -5;
			}
			
			GameObject m_bulletSoundObject;
			
			if (Time.timeScale != 0)
			{
				if (m_fireTimer <= 0)
				{
					if (m_bullet)
					{
						Instantiate (m_bullet, m_bulletSpawn.transform.position, m_bulletSpawn.transform.rotation);
					}
					
					if (m_bulletSound)
					{
						m_bulletSoundObject = Instantiate (m_bulletSound, m_bulletSpawn.transform.position, m_bulletSpawn.transform.rotation) as GameObject;
					}
					
					m_fireTimer = 1;
				}
			}
			
			m_fireTimer -= Time.deltaTime * m_fireRate;
		}
	}

	protected void SmartBomb ()
	{
		Frame m_controllerFrame = m_controller.Frame ();

		if (m_controllerFrame.IsValid)
		{
			FingerList m_fingerCount = m_controllerFrame.Fingers ;

			if (m_fingerCount.Count == 5)
			{
				if (m_smartBombAmount > 0)
				{
					if (m_bombTimer < -5)
					{
						m_bombTimer = -5;
					}
				
					GameObject m_bulletSoundObject;
				
					if (Time.timeScale != 0)
					{
						if (m_bombTimer <= 0)
						{
							if (m_smartBomb)
							{
								Instantiate (m_smartBomb, m_bulletSpawn.transform.position, m_bulletSpawn.transform.rotation);
								Instantiate (m_effects[0], m_bulletSpawn.transform.position, m_bulletSpawn.transform.rotation);
							}
						
							if (m_bulletSound)
							{
								m_bulletSoundObject = Instantiate (m_bulletSound, m_bulletSpawn.transform.position, m_bulletSpawn.transform.rotation) as GameObject;
							}
						
							m_bombTimer = 1;
							m_smartBombAmount -= 1;
						}
					}
				
					m_bombTimer -= Time.deltaTime * m_bombRate;
				}
			}
			else
			{
				m_bombTimer -= Time.deltaTime * m_bombRate;
			}
		}
	}

	protected virtual void Death ()
	{
		if (m_health <= 0)
		{
			if (m_life <=1)
			{
				Destroy (gameObject);
				Application.LoadLevel(m_lvlName);
			}
			else
			{
				m_health = 100;
				m_life = m_life - 1;
				GameObject m_effectObject = Instantiate (m_effects[2], m_bulletSpawn.transform.position, transform.rotation) as GameObject;
				m_effectObject.transform.parent = GameObject.Find("Player").transform;
				Instantiate (m_effectSound[0], m_bulletSpawn.transform.position, transform.rotation);
			}
		}
	}

	protected void Engage ()
	{
		Vector3 m_forRay = transform.TransformDirection(Vector3.right);
		RaycastHit hit;
		
		Debug.DrawRay (transform.position, m_forRay * M_VIEWDISTANCE, Color.red);
		
		if (Physics.Raycast (transform.position, m_forRay, out hit, M_VIEWDISTANCE, M_RAYCHECKID) && (hit.transform.gameObject.tag == M_RAYCHECKSHOOTER || hit.transform.gameObject.tag == M_RAYCHECKCHARGER))
		{
			m_engaged = true;
		}
		if (!Physics.Raycast (transform.position, m_forRay, M_VIEWDISTANCE, M_RAYCHECKID))
		{
			m_engaged = false; 
		}
	}

	protected void OnCollisionEnter (Collision other)
	{
		if (other.gameObject.tag == "EnemyBullet")
		{
			m_health = m_health - 10;

			GameObject m_effectObject = Instantiate (m_effects[1], other.gameObject.transform.position, transform.rotation) as GameObject;
			m_effectObject.transform.parent = GameObject.Find("Player").transform;
			Instantiate (m_effectSound[1], m_bulletSpawn.transform.position, transform.rotation);
		}
		if (other.gameObject.tag == "Shooter")
		{
			m_health = m_health - 10;
			GameObject m_effectObject = Instantiate (m_effects[1], other.gameObject.transform.position, transform.rotation) as GameObject;
			m_effectObject.transform.parent = GameObject.Find("Player").transform;
			Instantiate (m_effectSound[1], m_bulletSpawn.transform.position, transform.rotation);
		}
		if (other.gameObject.tag == "Charger")
		{
			m_health = m_health - 25;
			GameObject m_effectObject = Instantiate (m_effects[1], other.gameObject.transform.position, transform.rotation) as GameObject;
			m_effectObject.transform.parent = GameObject.Find("Player").transform;
			Instantiate (m_effectSound[1], m_bulletSpawn.transform.position, transform.rotation);
		}
		if (other.gameObject.tag == "HealthPickUp")
		{
			if (m_health < M_MAXHEALTH)
			{
				m_health = m_health + 5;
			}
		}
		if (other.gameObject.tag == "SmartBombPickUp")
		{
			if (m_smartBombAmount < M_MAXSMARTBOMB)
			{
				m_smartBombAmount = m_smartBombAmount + 1;
			}
		}
	}	

	protected Vector2 ProcessFinger (Frame m_currentFrame, Finger m_finger)
	{
		Vector m_stabilizedPosition = m_finger.StabilizedTipPosition;
		InteractionBox m_ibox = m_currentFrame.InteractionBox;
		Vector m_normalizedPosition = m_ibox.NormalizePoint(m_stabilizedPosition);
		
		return (new UnityEngine.Vector2 (m_normalizedPosition.x * UnityEngine.Screen.width, m_normalizedPosition.y * UnityEngine.Screen.height));
	}
	
	protected Vector2 ProcessHand (Frame m_currentFrame, Hand m_hand)
	{
		Vector m_stabilizedPosition = m_hand.StabilizedPalmPosition;
		InteractionBox m_ibox = m_currentFrame.InteractionBox;
		Vector m_normalizedPosition = m_ibox.NormalizePoint(m_stabilizedPosition);
		
		return (new UnityEngine.Vector2 (m_normalizedPosition.x * UnityEngine.Screen.width, m_normalizedPosition.y * UnityEngine.Screen.height));
	}

	void OnGUI() 
	{
		switch (m_life)
		{
		case 3:
			GUI.DrawTexture(new Rect(10, 10, m_health, 10), m_healthBarTexture[0]);
			break;
		case 2:
			GUI.DrawTexture(new Rect(10, 10, m_health, 10), m_healthBarTexture[1]);
			break;
		case 1:
			GUI.DrawTexture(new Rect(10, 10, m_health, 10), m_healthBarTexture[2]);
			break;
		}

		if (m_smartBombAmount != 0)
		{
			if (m_bombTimer <= 0.0f)
			{
				GUI.DrawTexture(new Rect(10, 30, (m_smartBombAmount * 10), 10), m_smartBombTexture[0]);
			}
			else
			{
				GUI.DrawTexture(new Rect(10, 30, (m_smartBombAmount * 10), 10), m_smartBombTexture[1]);
				GUI.Label (new Rect (50, 25, 100, 20), m_bombTimer.ToString ());
			}
		}
	}
}